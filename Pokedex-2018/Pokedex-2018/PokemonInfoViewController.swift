//
//  PokemonInfoViewController.swift
//  Pokedex-2018
//
//  Created by Kevin Fernando Canacuán Pasquel on 19/6/18.
//  Copyright © 2018 Sebastian Guerrero. All rights reserved.
//

import UIKit

class PokemonInfoViewController: UIViewController {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var weightTextField: UITextField!
    @IBOutlet weak var heightTextField: UITextField!
    
    var pokemon:Pokemon?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func imprimir(name:String, weight:Int, height:Int, sprites:Sprite){
        
      //  nameTextField = (pokemon?.name)?
        
       // weightTextField.text(pokemon?.weight)
       // heightTextField.text(pokemon?.height)
        
        
        
    }
    
    func fillData(){
        
        nameTextField.text = "\(pokemon?.name)"
        weightTextField.text = "\(pokemon?.weight)"
        heightTextField.text = "\(pokemon?.height)"
        
        
        if pokemon?.sprites == nil {
            
            let bm = Network()
            
            bm.get((pokemon?.id)!, completionHandler: { (imageR) in
                
                DispatchQueue.main.async {
                    self.pokemonImage.image = imageR
                }
                
            })
            
        } else {
            
            pokemonImage.image = pokemon.imagen
            
        }
        
    }
    
    

}
