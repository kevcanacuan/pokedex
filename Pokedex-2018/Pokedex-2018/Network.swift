//
//  Network.swift
//  Pokedex-2018
//
//  Created by Sebastian Guerrero F on 6/13/18.
//  Copyright © 2018 Sebastian Guerrero. All rights reserved.
//

import Foundation
import Alamofire

class Network {
  
    func getAllPokemon(completion:@escaping ([Pokemon]) -> () ) {
    
        var pokemonArray:[Pokemon] = [] //array para colocar todos los pokemons
        let group = DispatchGroup() //ayuda a manejar un grupo de cosas asincronas
        
        var imgArray:[Sprite] = []
        
        for i in 1...8 {
        
            group.enter()
            Alamofire.request("https://pokeapi.co/api/v2/pokemon/\(i)").responseJSON { response in
                
                guard let data = response.data else {
                    print("Error")
                    return
                }
                
                guard let pokemon = try? JSONDecoder().decode(Pokemon.self, from: data) else {
                    print("Error decoding Pokemon")
                    return
                }
                
                
                //print(pokemon)
                pokemonArray.append(pokemon)
                
                
                func getPokemonImage(url:String, completion:@escaping ([Sprite]) -> () ) {
                    
                    let url = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/\(i).png"
                    
                    Alamofire.request(url).responseJSON { response in
                        guard let image = response.data else {
                            print("Error decoding Pokemon")
                            return }
                        
                        imgArray.append(image)
                        
                        
                        
                    }
                    
                    
                    
                }
                

                
                group.leave()
               
                
            }
            
            
            
            
            
            
        }
        
        group.notify(queue: .main) {        //notify, espera que el grupo este vacio
            completion(pokemonArray)
        }
    
  }
    
    
    
    
    
  
}

//Cuando se tiene mas de un request, se debe crear un grupo donde ejecutarlos.
